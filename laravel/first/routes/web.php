<?php



/*

|--------------------------------------------------------------------------

| Web Routes

|--------------------------------------------------------------------------

|

| Here is where you can register web routes for your application. These

| routes are loaded by the RouteServiceProvider within a group which

| contains the "web" middleware group. Now create something great!

|

*/



Route::get('/', function () {

    return view('welcome');

});





Route::get('/admin-panel', function () {

   return view('admin.dashboard');

});



Route::resource('/admin-panel/account', 'admin\AcccountController');
Route::post('/admin-panel/account/{id}', 'admin\AcccountController@update');
Route::post('/admin-panel/account/status/{id}', 'admin\AcccountController@status');


Route::get('/admin-login', 'admin\HomeController@index');
// route to process the form
Route::post('/admin-login', 'admin\HomeController@doLogin');



Route::resource('/admin-panel/license', 'admin\LicenseController');
