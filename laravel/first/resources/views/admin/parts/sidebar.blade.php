  <!-- Left side column. contains the logo and sidebar -->

  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->

    <section class="sidebar">

      <!-- Sidebar user panel -->

      <div class="user-panel">

        <div class="pull-left image">

          <img src="{{ URL::to('/') }}/public/admin/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

        </div>

        <div class="pull-left info">

          <p>Alexander Pierce</p>

          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>

        </div>

      </div>

      <!-- search form -->

      

      <!-- /.search form -->

      <!-- sidebar menu: : style can be found in sidebar.less -->

      <ul class="sidebar-menu" data-widget="tree">

        <li class="treeview active">

          <a href="{{URL::to('/')}}/admin-panel/account">

            <i class="fa fa-edit"></i> <span>Account</span>

            <span class="pull-right-container">

              <i class="fa fa-angle-left pull-right"></i>

            </span>

          </a>

          <ul class="treeview-menu">

            <li><a href="{{URL::to('/')}}/admin-panel/account/create"><i class="fa fa-circle-o"></i> Add Account</a></li>

            <li><a href="{{URL::to('/')}}/admin-panel/account"><i class="fa fa-circle-o"></i> List Account</a></li>

          </ul>

        </li>

		

		<li class="treeview">

          <a href="{{URL::to('/')}}/admin-panel/license">

            <i class="fa fa-edit"></i> <span>License</span>

            <span class="pull-right-container">

              <i class="fa fa-angle-left pull-right"></i>

            </span>

          </a>

          <ul class="treeview-menu">

            <li><a href="{{URL::to('/')}}/admin-panel/license/create"><i class="fa fa-circle-o"></i> Add License</a></li>

            <li><a href="{{URL::to('/')}}/admin-panel/license"><i class="fa fa-circle-o"></i> List License</a></li>

          </ul>

        </li>

      </ul>

    </section>

    <!-- /.sidebar -->

  </aside>



  <!-- Content Wrapper. Contains page content -->

