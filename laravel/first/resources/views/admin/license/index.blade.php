@include('admin.parts.header')
<!-- Left side column. contains the logo and sidebar -->
@include('admin.parts.sidebar')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        
        <small></small>
      </h1>
      <ol class="breadcrumb">
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          
		  
          <!-- /.box -->

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">License List</h3>
            </div>
			<a href="{{ route('license.create') }}"><button type="button" class="btn btn-block btn-primary btn-lg">Add License</button></a>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Username</th>
				  <th>Year</th>
				  
				  <th width="280px">Action</th>
                </tr>
                </thead>
				
                <tbody>
				<?php $i=0; ?>
		 @foreach ($license as $key => $license1)

                  <td>{{ ++$i }}</td>
                  <td>@foreach ($account as $key => $account1)
                  <?php if($account1->id == $license1->userid){echo $account1->username;}
				  ?>
					@endforeach</td>
                  <td>{{ $license1->years }}</td>
				  <td>
				  <a href="{{ route('license.edit',$license1->id) }}"><button type="button" class="btn btn-block btn-info">Edit</button></a>
				 <form method="post" action="{{ route('license.destroy',$license1->id) }}" accept-charset="UTF-8" style="display:inline">
				  <input name="_method" type="hidden" value="DELETE">
				  {{ csrf_field() }}
				  <button type="submit" class="btn btn-block btn-danger">Delete</button>
				  </form>
				  </td>
                </tr>
				@endforeach
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  @include('admin.parts.footer')
