@include('admin.parts.header')
  <!-- Left side column. contains the logo and sidebar -->
  @include('admin.parts.sidebar')


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">License</a></li>
        <li class="active">Add License</li>
      </ol>
    </section>

    <!-- Main content -->
    
    <!-- /.content -->
	<section class="content">
      <div class="row">
        <!-- left column -->
        
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add License</h3>
            </div>
            <!-- /.box-header -->
			
			@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

			
            <!-- form start -->
            <form role="form" method="post" action="{{ route('license.store') }}" enctype="multipart/form-data">
              <div class="box-body">
                
                
				<div class="form-group">
                  <label>Select Username</label>
                  <select class="form-control" name="userid">
				   @foreach ($account as $key => $account1)
                    <option value="{{ $account1->id }}">{{ $account1->name }}</option>
					@endforeach
                  </select>
                </div>


				

				<div class="form-group">
				<label for="exampleInputEmail1">Year</label>
                  <input type="text" class="form-control" id="year" name="years" placeholder="Year">
				  {{ csrf_field() }}
                </div>
				
				
				
                
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>

        </div>
		
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
  </div>
  <!-- /.content-wrapper -->
  @include('admin.parts.footer')
