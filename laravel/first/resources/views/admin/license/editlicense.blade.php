@include('admin.parts.header')
  <!-- Left side column. contains the logo and sidebar -->
  @include('admin.parts.sidebar')


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit License
        <small></small>
      </h1>
      
    </section>

    <!-- Main content -->
    
    <!-- /.content -->
	<section class="content">
      <div class="row">
        <!-- left column -->
        
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit License</h3>
            </div>
            <!-- /.box-header -->
			
			@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
			
			
            <!-- form start -->
            <form role="form" method="post" action="{{ route('license.update',$license->id) }}" enctype="multipart/form-data">
              <div class="box-body">
                
                
				<div class="form-group">
                  <label>Select</label>
                  <select class="form-control" name="username">
				   @foreach ($account as $key => $account1)
                    <option value="{{ $account1->id }}" {{ $license->userid == $account1->id ? 'selected' : '' }}>{{ $account1->name }}</option>
					@endforeach
                  </select>
                </div>
				
				<div class="form-group">

                  <label for="exampleInputEmail1">Year</label>
				  <input type="text" class="form-control" id="years" name="years" value="{{$license->years}}" placeholder="Year">
                  <input name="_method" type="hidden" value="PATCH">
				 {{ csrf_field() }}
                </div>
                
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Update</button>
              </div>
            </form>
          </div>

        </div>
		
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
  </div>
  <!-- /.content-wrapper -->
  @include('admin.parts.footer')
