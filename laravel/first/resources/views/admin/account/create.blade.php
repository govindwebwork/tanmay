@include('admin.parts.header')

  <!-- Left side column. contains the logo and sidebar -->

  @include('admin.parts.sidebar')


	<?php
	$string = str_random(15);
	
	// Available alpha caracters
$characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

// generate a pin based on 2 * 7 digits + a random character
$pin = mt_rand(10000000, 99999999)
    . mt_rand(1000000, 9999999)
    . $characters[rand(0, strlen($characters) - 1)];

// shuffle the result
$string = str_shuffle($pin);
	
	?>




  <!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">

    <!-- Content Header (Page header) -->

    <section class="content-header">

      <h1>

        

        <small></small>

      </h1>

      <ol class="breadcrumb">

        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

        <li><a href="#">Account</a></li>

        <li class="active">Add Account</li>

      </ol>

    </section>



    <!-- Main content -->

    

    <!-- /.content -->

	<section class="content">

      <div class="row">

        <!-- left column -->

        

        <div class="col-md-12">

          <!-- general form elements -->

          <div class="box box-primary">

            <div class="box-header with-border">

              <h3 class="box-title">Add Account</h3>

            </div>


@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
            <!-- /.box-header -->

            <!-- form start -->

            <form role="form" method="post" action="{{ route('account.store') }}" enctype="multipart/form-data">

              <div class="box-body">

                <div class="form-group">

                  <label for="exampleInputEmail1">Name</label>

                  <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" placeholder="Name">

				   {{ csrf_field() }}

                </div>

                

				<div class="form-group">

				<label for="exampleInputEmail1">E-Mail</label>

                  <input type="text" class="form-control" id="email" name="email" value="{{ old('email') }}" placeholder="E-Mail">

                </div>



				<div class="form-group">

				<label for="exampleInputEmail1">Contact Number</label>

                  <input type="text" class="form-control" id="contact" name="contact" value="{{ old('contact') }}" placeholder="Contact Number">

                </div>
				
				<div class="form-group">

				<label for="exampleInputEmail1">Sub-Domain</label>

                  <input type="text" class="form-control" id="subdomain" name="subdomain" value="{{ old('subdomain') }}" placeholder="Sub-Domain">

                </div>



				<div class="form-group">

				<label for="exampleInputEmail1">License Keys</label>

                  <input type="text" class="form-control" id="licensekey" name="licensekey" value="<?php echo $string; ?>" placeholder="License Keys">

                </div>

				

				<div class="form-group">

				<label for="exampleInputEmail1">Username</label>

                  <input type="text" class="form-control" id="username" value="{{ old('username') }}"  name="username" placeholder="Username">

                </div>

				<div class="form-group">

				<label for="exampleInputEmail1">Password</label>

                  <input type="text" class="form-control" id="password" value="{{ old('password') }}" name="password" placeholder="Password">

                </div>





                <!--<div class="form-group">

                  <label for="exampleInputFile">File input</label>

                  <input type="file" name="image" id="exampleInputFile">



                  </div>-->

				

				<div class="box-body pad">

              <label for="exampleInputEmail1">Address</label>

                    <textarea id="editor1" name="address" rows="10" cols="20" style="visibility: hidden; display: none;"></textarea>

              

            </div>

                

              </div>

              <!-- /.box-body -->



              <div class="box-footer">

                <button type="submit" class="btn btn-primary">Submit</button>

              </div>

            </form>

          </div>



        </div>

		

        <!--/.col (right) -->

      </div>

      <!-- /.row -->

    </section>

  </div>

  <!-- /.content-wrapper -->

  @include('admin.parts.footer')

