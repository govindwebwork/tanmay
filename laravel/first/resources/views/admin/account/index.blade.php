@include('admin.parts.header')

<!-- Left side column. contains the logo and sidebar -->

@include('admin.parts.sidebar')

<!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">

    <!-- Content Header (Page header) -->

    <section class="content-header">

      <h1>

        Account Tables

        <small>advanced tables</small>

      </h1>

      

    </section>



    <!-- Main content -->

    <section class="content">

      <div class="row">

        <div class="col-xs-12">

          

		  

          <!-- /.box -->

@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif

          <div class="box">

            <div class="box-header">

              <h3 class="box-title">Account List</h3>

            </div>

			<a href="{{ route('account.create') }}"><button type="button" class="btn btn-block btn-primary btn-lg">Add Account</button></a>

            <!-- /.box-header -->

            <div class="box-body">

              <table id="example1" class="table table-bordered table-striped">

                <thead>

                <tr>

                  <th>ID</th>

                  <th>Name</th>

				  <th>Email</th>

				 <th>Username</th>
				  

				  <th>Sub-Domain</th>

				  <th width="280px">Action</th>

                </tr>

                </thead>

				

                <tbody>
								<?php $i=0; ?>
		 @foreach ($account as $key => $account1)

                <tr>
                  <td>{{ ++$i }}</td>
                  <td>{{ $account1->name }}</td>
				  <td>@foreach ($user as $key => $user1)
                  <?php if($account1->userid == $user1->id){echo $user1->email;}
				  ?>
					@endforeach
					</td>
                  <td>{{ $account1->username }}</td>
                  <td>{{ $account1->subdomain }}</td>
				  <td>
				  <a href="{{ route('account.edit',$account1->id) }}"><button type="button" class="btn btn-block btn-info">Edit</button></a>
				 <form method="post" action="{{ route('account.destroy',$account1->userid) }}" accept-charset="UTF-8" style="display:inline">
				  <input name="deleteid" type="hidden" value="{{$account1->userid}}">
				  <input name="_method" type="hidden" value="DELETE">
				  {{ csrf_field() }}
				  <button type="submit" class="btn btn-block btn-danger">Delete</button>
				  </form>
				  
				  
				  <?php if($account1->status == 0){?>
				  <form method="post" action="{{ URL::to('admin-panel/account/status',$account1->id) }}" accept-charset="UTF-8" style="display:inline">
				  <input name="statusid" type="hidden" value="1">
				  
				  {{ csrf_field() }}
				  <button type="submit" class="btn btn-block btn-success" data-toggle=confirmation>Active</button>
				  </form>
				  <?php } else {?>
				  <form method="post" action="{{ URL::to('admin-panel/account/status',$account1->id) }}" accept-charset="UTF-8" style="display:inline">
				  <input name="statusid" type="hidden" value="0">
				 
				  {{ csrf_field() }}
				  <button type="submit" class="btn btn-block btn-danger">Deactive</button>
				  </form>
				  <?php } ?>
				  </td>
                </tr>
			@endforeach

                </tfoot>

              </table>

            </div>



			<script type="text/javascript">
				$(document).ready(function () {        
					$('[data-toggle=confirmation]').confirmation({
						rootSelector: '[data-toggle=confirmation]',
						onConfirm: function (event, element) {
							element.closest('form').submit();
						}
					});   
				});
			</script>
            <!-- /.box-body -->

          </div>

          <!-- /.box -->

        </div>

        <!-- /.col -->

      </div>

      <!-- /.row -->

    </section>

    <!-- /.content -->

  </div>

  <!-- /.content-wrapper -->

  @include('admin.parts.footer')
