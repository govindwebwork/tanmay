<?php



namespace App\Http\Controllers\Admin;


use Auth;
use App\Http\Controllers\Controller;

use Redirect;

use Schema;
use App\User;

use App\Http\Requests\CreateProductRequest;

use App\Http\Requests\UpdateProductRequest;

use Illuminate\Http\Request;

use App\Http\Controllers\Traits\FileUploadTrait;

use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Contracts\Session\Session;


class HomeController extends Controller

{

	use ValidatesRequests;

    public function index()
    {
	
        return view('admin.login');
    }

		public function doLogin()
		{
		// validate the info, create rules for the inputs
		$rules = array(
			'email'    => 'required|email', // make sure the email is an actual email
			'password' => 'required|alphaNum|min:3' // password can only be alphanumeric and has to be greater than 3 characters
		);
		
		// run the validation rules on the inputs from the form
		$validator = Validator::make(Input::all(), $rules);
		
		// if the validator fails, redirect back to the form
		if ($validator->fails()) {
			return Redirect::to('admin-login')
				->withErrors($validator) // send back all errors to the login form
				->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
		} else {
		
			// create our user data for the authentication
			$userdata = array(
				'email'     => Input::get('email'),
				'user_type'  => '1',
				'password'  => Input::get('password')
			);
		
			// attempt to do the login
			if (Auth::attempt($userdata)) {
		
				// validation successful!
				// redirect them to the secure section or whatever
				// return Redirect::to('secure');
				// for now we'll just echo success (even though echoing in a controller is bad)
				//session()->put('email', $userdata['email']);
				//echo 'SUCCESS!';
				//echo session()->get('email');
				return Redirect::to('admin-panel');
		
			} else {        
		
				// validation not successful, send back to form 
				//return Redirect::to('admin-login');
				return Redirect::to('admin-login');
		
			}
		
		}
		}




}

