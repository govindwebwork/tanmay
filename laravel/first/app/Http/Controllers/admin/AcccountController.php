<?php



namespace App\Http\Controllers\Admin;



use App\Http\Controllers\Controller;
use Hash;
use Redirect;
use Schema;
use App\Account;
use App\User;

use App\Http\Requests\CreateProductRequest;

use App\Http\Requests\UpdateProductRequest;

use Illuminate\Http\Request;

use App\Http\Controllers\Traits\FileUploadTrait;

use Illuminate\Foundation\Validation\ValidatesRequests;



class AcccountController extends Controller

{

	use ValidatesRequests;


	public function index()

    {

        
		$account = Account::orderBy('id','DESC')->get();
		$user = User::orderBy('Id','DESC')->get();
		return view('admin.account.index',compact('account','user'));

	}

	public function create()

	{
	    return view('admin.account.create');
	}

	public function store(Request $request)

	{
	
		$request->validate([
		'name' => 'required|regex:/^[\pL\s\-]+$/u',
		'email' => 'required|unique:users|max:255',
		'contact' => 'required|numeric',
		'subdomain' => 'required|unique:accounts|regex:/^([a-zA-Z0-9][a-zA-Z0-9-_]*\.)*[a-zA-Z0-9]*[a-zA-Z0-9-_]*[[a-zA-Z0-9]+$/im',
		'licensekey' => 'required',
		'username' => 'required|unique:accounts|max:255',
		'password' => 'required',
	]);

	 $user = new User($request->input()) ;
      $user->name = $request->input('username');
      $user->email = $request->input('email');
      $password = $request->input('password');
	 	$hashed = Hash::make($password);
      $user->password = $hashed;
		$user->save();

	 $account = new Account($request->input()) ;
	 $lastInsertedId = $user->id;
      $account->userid = $lastInsertedId;
      $account->name = $request->input('name');
      $account->contact = $request->input('contact');
      $account->subdomain = $request->input('subdomain');
      $account->licensekey = $request->input('licensekey');
      $account->address = $request->input('address');
      $account->save();

		return redirect()->route('account.index')->with('message', 'Account added Successfully');

	}


	 public function edit($id)
    {
        $account = Account::find($id);
		$user = User::orderBy('Id','DESC')->get();
        return view('admin.account.editaccount',compact('account','user'));
    }





	public function destroy(Request $request)
		{
		
			$deleteid = $request->input('deleteid');
			$affectedRows =Account::where('userid', '=', $deleteid)->delete();
			//DB::table('accounts')->where('userid', '=', $deleteid)->delete()
			User::find($deleteid)->delete();
			//Account::find($id)->delete();
			return redirect()->route('account.index')->with('status', 'Account Delete Successfully');
		}




		public function update(Request $request, $id)
			{
				$user = User::find($id);
				$user->name = $request['username'];
				$user->email = $request['email'];
				$user->save();
		
				$account = Account::where('userid',$id)->first();
				$account->name = $request['name'];
				$account->contact = $request['contact'];
				$account->subdomain = $request['subdomain'];
				$account->licensekey = $request['licensekey'];
				$account->username = $request['username'];
				$account->address = $request['address'];
				$account->status = $request['status'];
				$account->save();
				
				return redirect()->route('account.index');
			}



	public function status(Request $request, $id)
    {
        $account = Account::find($id);
		$account->status = $request->input('statusid');
		$account->save();
		return redirect()->route('account.index')->with('status', 'Account Status Change Successfully');
    }


}

