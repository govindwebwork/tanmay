<?php



namespace App\Http\Controllers\Admin;



use App\Http\Controllers\Controller;
use Hash;
use Redirect;

use Schema;

use App\Account;
use App\User;

use App\License;


use App\Http\Requests\CreateProductRequest;

use App\Http\Requests\UpdateProductRequest;

use Illuminate\Http\Request;

use App\Http\Controllers\Traits\FileUploadTrait;

use Illuminate\Foundation\Validation\ValidatesRequests;



class LicenseController extends Controller

{

	use ValidatesRequests;

	public function index()

    {

        
		$account = Account::orderBy('id','DESC')->get();
		$license = License::orderBy('id','DESC')->get();
		return view('admin.license.index',compact('account','license'));

	}

	public function create()

	{
		$account = Account::orderBy('id','DESC')->get();
	    return view('admin.license.create',compact('account'));
	}


	public function store(Request $request)

	{
	
		$request->validate([
		'years' => 'required|min:1|max:99|numeric',
	]);

		$license = new License($request->input()) ;
        $license->save() ;

		return redirect()->route('license.index')->with('message', 'License Year added Successfully');;

	}

	public function destroy($id)
    {
        License::find($id)->delete();
        return redirect()->route('license.index');
    }

	 public function edit($id)
    {
        $license = License::find($id);
		$account = Account::orderBy('id','DESC')->get();
        return view('admin.license.editlicense',compact('account','license'));
    }



	public function update(Request $request, $id)
    {
		$request->validate([
		'years' => 'required|min:1|max:99|numeric',
	]);
	  $license = License::find($id);
      // replace old data with new data from the submitted form //
      $license->userid = $request->input('username');
      $license->years = $request->input('years');
      $license->save();
	  //Category::find($id)->update($request->all());
	   return redirect()->route('license.index');
    }


}

